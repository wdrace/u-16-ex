# CS235/CS250 Unit 02 Exercise: CS 200 Review

```
--------------------------------------------------------------------------------
MAIN MENU
--------------------------------------------------------------------------------

Current list:

Options:
0. QUIT
1. Load to-do list
2. Save to-do list
3. Add item to list
4. Update existing item
5. Remove item from list
-> 

```



## Introduction

This assignment is meant to help you review core CS 200 topics. If you feel rusty at any of the parts covered, please review them this week.

### Example program output

------------------------------------------------------------------

## Program instructions

### Creating the project

1. Create a project in the IDE of your choice, such as Visual Studio or Code::Blocks.
2. Create the following files in your project: `main.cpp`, `Program.h`, and `Program.cpp`.

### Creating the Program class

We will be creating the Program class declaration with the `Program.h` file.

1. Create the file guards within `Program.h`. The file guards look like this:

```
#ifndef _PROGRAM
#define _PROGRAM

#endif
```

2. The UML diagram for the Program class is as follows:

| Program                                |
| -------------------------------------- |
| VARIABLES:                             |
| - m_todoItems : vector<string>         |
| FUNCTIONS:                             |
| + Run() : void                         |
| + Menu_Main() : void                   |
| + Menu_LoadList() : void               |
| + Menu_SaveList() : void               |
| + Menu_AddToList() : void              |
| + Menu_UpdateListItem() : void         |
| + Menu_RemoveFromList() : void         |
| + GetChoice( min: int, max:int ) : int |
| + DisplayHeader( text: string ) : void |
| + DisplayList() : void                 |

The first row of the diagram is the class name. The second row is the member variables. The third row is the member functions.

UML is language agnostic and items are written as NAME : TYPE, so for instance the variable `count : int` would be declared as `int count;`.

Write the class declaration within `Program.h`. Note that the class includes a `vector` and a `string`, so you also need to have `#include <vector>` and `#include <string>` at the top of your file.

### Creating the stubs for the Program's functions

Function definitions will go in the `Program.cpp` file. Within this file, make sure to have `#include "Program.h"`. (NOTE: Never #include cpp files!)

Create stubs for each of the functions to start off with, we will implement them later. It will look like this:

```
#include "Program.h"

void Program::Run()
{
}

void Program::Menu_Main()
{
}

void Program::Menu_LoadList()
{
}

void Program::Menu_SaveList()
{
}

void Program::Menu_AddToList()
{
}

void Program::Menu_UpdateListItem()
{
}

void Program::Menu_RemoveFromList()
{
}

void Program::DisplayList()
{
}

void Program::DisplayHeader( string text )
{
}

int Program::GetChoice( int min, int max )
{
    return -1; // placeholder
}
```

### Creating main()

Within the `main.cpp` file make sure you're also including the Program.h file here. Within the main() function, create a Program variable and call its Run() function:

```
#include "Program.h"

int main()
{
    Program program;
    program.Run();

    return 0;
}
```

Build the program - there should be no syntax errors. The program should run, but nothing will show up for now.

### Implementing the Program's functions (part 1)

#### void Program::DisplayHeader( string text )

Display the header `text` in a way that makes the header stand out. For example:

```C++
cout << string( 80, '-' ) << endl;
cout << text << endl;
cout << string( 80, '-' ) << endl << endl;
```



#### int Program::GetChoice( int min, int max )

Prompt the user to enter an integer value. Then, use a while loop to continue asking them to re-enter their selection **while their selection is invalid**. (The selection is invalid if it is less than `min` or greater than `max`.)

After the while loop the selection should be valid so then return the user's choice.



#### void Program::Run()

This function should just call `Menu_Main()`.



#### void Program::Menu_Main()

Create a program loop, for example:

```C++
bool done = false;
while ( !done )
{
    cout << "MENU" << endl;
}
```

Within the program loop, do the following:

1. Call the `DisplayHeader` function, passing in a title like "MAIN MENU".

2. Call the `DisplayList` function.

3. Display a numbered list of menu options: Quit, Save, Load, Add item, Update existing item, and Remove item.

4. Get the user's selection by calling the `GetChoice` function.

5. Use if statements or a switch statement to decide what to do based on the user's selection:
   
   1. For quit, set `done = true`.
   
   2. For the others, call the corresponding Menu_ function.



#### void Program::Menu_LoadList()

Just call `DisplayHeader`, displaying "LOAD LIST".



#### void Program::Menu_SaveList()

Just call `DisplayHeader`, displaying "SAVE LIST"



#### void Program::Menu_AddToList()

Just call `DisplayHeader`, displaying "ADD ITEM TO LIST"



#### void Program::Menu_UpdateListItem()

Just call `DisplayHeader`, displaying "UPDATE ITEM"



#### void Program::Menu_RemoveFromList()

Just call `DisplayHeader`, displaying "REMOVE ITEM FROM LIST"



#### void Program::DisplayList()

HINTS:

* Refer to "Iterate over object array" for an example of iterating over an array or vector object: [Quick Reference (Course Commons)](https://rachels-courses.gitlab.io/webpage/ref/reference.html#iterate-array-obj) 



Use a for loop to iterate over all the elements of the `m_todoList` vector, displaying each element's **index** and **value**.





#### Testing the program

The program's main menu should now work, and when you go to each area you should see its header, even though the result of the functionality isn't implemented yet.

```
--------------------------------------------------------------------------------
MAIN MENU
--------------------------------------------------------------------------------

Current list:

Options:
0. QUIT
1. Load to-do list
2. Save to-do list
3. Add item to list
4. Update existing item
5. Remove item from list
-> 1
--------------------------------------------------------------------------------
LOAD LIST
--------------------------------------------------------------------------------
```



### Implementing the Program's functions (part 2)

Next we will implement the Add and Save functionality.



#### void Program::Menu_AddToList()

HINTS:

* Refer to the vector documentation on how to use `push_back`: https://cplusplus.com/reference/vector/vector/push_back/

* Remember that when switching between `cin >>` and `getline`, you need to put a `cin.ignore();` in-between. See also **"Get a full line of text (with spaces) and store in a variable (string only):"** at: [Quick Reference (Course Commons)](https://rachels-courses.gitlab.io/webpage/ref/reference.html#ref-cincout)



Within the AddToList function, do the following:

1. Create a `string` variable to store the user's entry;

2. Ask the user to enter a new "to-do item". Use the `getline` command to receive a full line of text from them.

3. Use the `push_back` vector function to push the new item to the `m_todoItems` vector.

4. Display a verification - "Item added."



#### void Program::Menu_SaveList()

HINT: 

* Since this function uses `ofstream`, make sure you have `#include <fstream>` at the top of the file.

* Refer to the `ofstream` documentation for how to open a file: https://cplusplus.com/reference/fstream/ofstream/open/



Within this function, do the following:

1. Create a string to store the filename. Ask the user to enter the filename and store it in this string.

2. Create an `ofstream` variable named `output`. Open the filename.

3. Use a for loop to iterate over all items in `m_todoItems`. Within the for loop:
   
   1. Output the `m_todoItems` element at that index to the `output` file. (Don't output the index.)

4. After the for loop is over, display a confirmation, "File saved."



#### Testing the program

When you run the program you should be able to add items and then use the save command.

```
--------------------------------------------------------------------------------
ADD TO LIST
--------------------------------------------------------------------------------

Enter to-do item: Eat lunch

Item added.

```



```
--------------------------------------------------------------------------------
SAVE LIST
--------------------------------------------------------------------------------

Filename to save to: daily.txt

File saved.

```



After running the program, the filename you specified **should be generated in the location where your project file is.** For Visual Studio, this will be the location of your .vcxproj. For Code::Blocks, it will be the location of the .cbp file.

Open up the text file and make sure it has contents:

```
Eat breakfast
Eat lunch
```



### Implementing the Program's functions (part 3)

Let's implement the rest of the functionality.



#### void Program::Menu_LoadList()

HINTS:

* You can use the `fail()` ifstream function (which returns a boolean) to see if opening the file failed or not. It returns `true` if there was a failure. https://cplusplus.com/reference/ios/ios/fail/

* You can read an entire file one line at a time like this:
  ` while ( getline( input, line ) ) {`



For this function do the following:

1. Create a filename string and ask the user to enter a filename. Store it in this string.

2. Create an `ifstream` variable named `input`. Try to open the filename given.

3. IF opening the file failed, then display an error and return.

4. Otherwise, create another string variable to store one `line` of the file at a time.

5. Create a while loop that continues while using `getline` on the `input` is successful. Within the while loop:
   
   1. Use the `push_back` function of the `m_todoItems` vector to add the `line` to the to do list.

6. After the while loop display a confirmation message: "File loaded."



(Go ahead and run the program and run LOAD to make sure it can load the file you just saved earlier!)





#### void Program::Menu_UpdateListItem()

HINTS:

* The valid indices of a vector is a minimum of 0, a maximum of `.size() - 1`.

* You can access elements of a vector with the subscript operator `[ ]`: https://cplusplus.com/reference/vector/vector/operator[]/



Within this function do the following:

1. Call the `DisplayList` function.

2. Ask the user "Update which index?". Create an integer named `index` to store their answer, and call the `GetChoice` function to ensure they enter something valid.

3. Ask the user "Update to what?". Create a string variable named `updateText` to store their answer. Make sure to use `getline` to allow a full line of text.

4. Update the element of `m_todoItems` at the given `index` to the value from `updateText`.

5. Display a confirmation: "Item updated."



#### void Program::Menu_RemoveFromList()

HINTS:

* The vector object has a `erase` function you can use to remove items at an index: https://cplusplus.com/reference/vector/vector/erase/



Within this function do the following:

1. Call the `DisplayList` function.

2. Ask the user "Remove which index?". Create an integer `index` to store their answer. Call the `GetChoice` function to ensure that their index is within a valid range.

3. Use the vector's `erase` function to remove the item at that `index` from the `m_todoItems` vector. Note that the argument will be `m_todoItems.begin() + index` because C++ is old and weird.

4. Display a confirmation: "Item removed."



#### Testing the program

All functionality of the program should now work properly. Run the program, try adding, updating, and removing items. Save and load. 








