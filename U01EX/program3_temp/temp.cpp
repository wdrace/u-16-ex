#include <iostream>
using namespace std;
int main(int argumentCount, char *argumentList[]) {
  if (argumentCount < 3) {
    cout << "NOT ENOUGH ARGUMENTS! Expected form:" << endl;
    cout << argumentList[0] << " temp C/F" << endl;
    return 1;
  }

  float temp = stof(argumentList[1]);
  char unit = toupper(argumentList[2][0]);
  float resultTemp = -100;
  char resultUnit = 'Z';

  if (unit == 'C') {
    // Celsius to Farenheit
    resultUnit = 'F';
    resultTemp = (9 / 5) * temp + 32;
  } else {
    // Farenheit to Celsius
    resultUnit = 'C';
    resultTemp = (temp - 32) * (5.0 / 9.0);
  }

  cout << temp << " " << unit << " = " << resultTemp << " " << resultUnit
       << endl;

  return 0;
}