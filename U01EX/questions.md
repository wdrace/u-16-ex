# Questions

1. (PROGRAM 1 - ZORK) What were your impressions of the interface? Was it easy to use? Hard to use? What would you have changed about it to make it more user friendly?

2. (PROGRAM 2 - SALE) What were your impressions of the interface? Was it easy to use? Hard to use? What would you have changed about it to make it more user friendly?

3. (PROGRAM 3 - TEMPERATURE) What were your impressions of the interface? Was it easy to use? Hard to use? What would you have changed about it to make it more user friendly?

4. Which style of program (continuously running or command line with arguments) do you think would be more suitable for SMALL, one-action programs? What about larger programs that can perform many different types of operations?

5. (PROGRAM 4 - BAZARRE) The Bazarre program has a GUI (Graphical User Interface) that you can navigate with the mouse or with the keyboard (WASD to move cursor, J to select). Give some PROS and CONS of a GUI over the previous programs' CLI (Command Line Interface). In what types of applications might a CLI be preferred over a GUI
