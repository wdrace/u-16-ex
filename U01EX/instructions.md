--------------------------------------------------------

INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

For this assignment we will be exploring already existing software
and comparing how the users interface with them.

As you work through this assignment, make sure to fill out the
questions.txt file.

--------------------------------------------------------

PROGRAM INSTRUCTIONS
--------------------------------------------------------

### PROGRAM 1: CONTINUOUSLY RUNNING - ZORK

Run the game at this link: [Zork I | ClassicReload.com](https://classicreload.com/zork-i.html)

Zork is a game that came out in the 1980s. It runs in the console (the black window with white text) and is text-only. Spend a few minutes playing the game and exploring. 

You can type in commands like: NORTH, SOUTH, EAST, WEST to move around,
OPEN (object), TAKE (object), READ (object) and more to interact with the environment.
(You can see a list of commands here: https://zork.fandom.com/wiki/Command_List)

Answer the questions in questions.txt before continuing.

--------------------------------------------------------

### Info: Opening a shell

In WINDOWS, you can open the POWER SHELL in a directory by holding the SHIFT KEY and RIGHT-CLICKING in some empty space within a folder. The Powershell option should then show up.

![Screenshot](powershell.png)

In LINUX, open the TERMINAL and navigate to this exercise's directory

--------------------------------------------------------

### Troubleshooting: Having trouble running PROGRAM 2 and PROGRAM 3?

If Program 2 and Program 3 aren't running, try to disable your virus scanner first.
If that doesn't work, you can work with the same files on this replit:

https://replit.com/@rsingh13/Unit-01-Exercise-Exploring-software-CS250U01EX

![Screenshot](replit.png)

Use the following commands to navigate:

* `ls` to LiSt files/folders in the current directory
* `cd FOLDERNAME` to navigate to another directory
* `cd ..` to go back/up one directory
* `./PROGRAMNAME` to run a program

So to run each program:

```
cd program2_sale
ls
./sale_linux
cd ..
cd program3_temp
./temp_linux 100 F
```

--------------------------------------------------------

### PROGRAM 2: CONTINUOUSLY RUNNING - SALE

Run the sale program by typing the following in the SHELL or CONSOLE:

* WINDOWS: `.\sale_win.exe`
* LINUX: `./sale_linux`

**⚠️ IF THE PROGRAM DOES NOT BEGIN, TRY TO TEMPORARILY DISABLE YOUR VIRUS SCAN. Your Virus Scanner is probably preventing it from running. ⚠️**

This will begin the program, which looks like this:

```
TOTAL PRICE:     $0.00
TAX:             $0.00
PRICE AFTER TAX: $0.00

1. Clear shopping cart
2. Add another item
3. Exit

>> 
```

Answer the questions in questions.txt before continuing.

If you would like, you can view the source code for this program. It is the "sale.cpp" file.

--------------------------------------------------------

### PROGRAM 3: COMMAND LINE WITH ARGUMENTS - TEMPERATURE CONVERSION (C++)

Zork and the Sale programs run until the user decides to quit. With these console utility programs they usually take in some input, run an operation, and immediately quit.

For these programs, open up the Windows PowerShell (or your OS' terminal for Linux/Mac):

This program can be run with the same `./` command, but it also requires two input arguments:

* WINDOWS: `.\temp_win.exe TEMPERATURE C/F`
* LINUX: `./temp_linux TEMPERATURE C/F`

If you don't give any information besides the program name, it will complain:

```
$ ./temp
NOT ENOUGH ARGUMENTS! Expected form:
./temp temp C/F
```

Try running the program to convert 0 C to Farenheit like this:

```
./temp 0 C
0 C = 32 F
```

Or try to convert 100 F to Celsius like this:

```
./temp 100 F
100 F = 37.7778 C
```

Answer the questions in questions.txt before continuing.

If you would like, you can view the source code for this program. It is the "temp.cpp" file.

--------------------------------------------------------

### PROGRAM 4: GRAPHICAL PROGRAM - BAZARRE (C++)

This program is written with C++ along with a third party library, SFML, which allows for cross-platform window, graphics, audio management and more.

You can run the WINDOWS version by opening "gamejam-windows-release.exe".

You can run the LINUX version by running "gamejam-RUN-LINUX.sh".

Answer the questions in questions.txt after running.
